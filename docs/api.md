# Bowling REST API

# Games [/games/]
This endpoint is used to view all games and create new games.

## Retrieve Games [GET]
Retrieve all games.

+ Response 200

[
    {
        "id": 30,
        "players": [
            {
                "current_player": false,
                "frames": [
                    {
                        "number": 1,
                        "rolls": [
                            {
                                "pins_dropped": 10
                            },
                            {
                                "pins_dropped": 10
                            },
                            {
                                "pins_dropped": 10
                            }
                        ],
                        "score": 30,
                        "status": "S"
                    }
                ],
                "name": "bob",
                "order": 0,
                "score": 30
            }
        ],
        "status": "F"
    },
    {
        "id": 31,
        "players": [
            {
                "current_player": false,
                "frames": [
                    {
                        "number": 1,
                        "rolls": [
                            {
                                "pins_dropped": 6
                            },
                            {
                                "pins_dropped": 4
                            },
                            {
                                "pins_dropped": 10
                            }
                        ],
                        "score": 20,
                        "status": "S"
                    }
                ],
                "name": "tom",
                "order": 0,
                "score": 20
            }
        ],
        "status": "F"
    },
    {
        "id": 32,
        "players": [
            {
                "current_player": false,
                "frames": [],
                "name": "John",
                "order": 0,
                "score": 0
            }
        ],
        "status": "NS"
    }
]

## Create Game [POST]
Create a new game.

+ Response 201
{
    "id": 33,
    "players": [],
    "status": "NS"
}

# Game [/games/{id}/]
This resource represents a game.

## Retrieve Game [GET]
Retrieve a game by its *id*.

+ Response 200

{
    "id": 31,
    "players": [
        {
            "current_player": false,
            "frames": [
                {
                    "number": 1,
                    "rolls": [
                        {
                            "pins_dropped": 6
                        },
                        {
                            "pins_dropped": 4
                        },
                        {
                            "pins_dropped": 10
                        }
                    ],
                    "score": 20,
                    "status": "S"
                }
            ],
            "name": "tom",
            "order": 0,
            "score": 20
        }
    ],
    "status": "F"
}

# Players [/games/{id}/players/]
This endpoint is used to view the players in a game and add new players.

## Retrieve Players [GET]
Retrieve the players in a game. The game is specified by its *id*.

+ Response 200

[
    {
        "current_player": false,
        "frames": [],
        "name": "John",
        "order": 0,
        "score": 0
    },
    {
        "current_player": false,
        "frames": [],
        "name": "Tom",
        "order": 1,
        "score": 0
    }
]

## Create Player [POST]
Create a player in the game. Game must not have started. Payload should include a field 'name' specifying the player's name.

+ Request (application/json)

{
    "name": "Player Name"
}

+ Response 201

{
    "current_player": false,
    "frames": [],
    "name": "Tom",
    "order": 1,
    "score": 0
}

# Player [/games/{id}/players/{name}]
This resource represents a player.

## Retrieve Player [GET]
Retrieve a player in a game by his or her name.

+ Response 200

{
    "current_player": false,
    "frames": [],
    "name": "Tom",
    "order": 1,
    "score": 0
}

# Start [/games/{id}/start/]
This endpoint is used to start a game.

## Start Game [POST]
Starts the game identified by *id*. The game must have at least one player before it can be started.

+ Response 200

{
    "next roll": "John"
}

# Roll [/games/{id}/roll/{pins}]
This endpoint is used to apply a roll to a game score.

## Apply Roll [POST]
Applies a roll to the game identified by *id*. The value of pins represents the number of pins knocked down by the roll.

+ Response 200

{
    "next roll": "John"
}


import json

from rest_framework import status
from rest_framework.response import Response
from django.http import Http404
from django.db.models import Max

from games.serializers import PlayerSerializer
from games.views.game_views import GameView
from games.models import Player


class PlayerList(GameView):
    """
    List players in a game, or add a player to a game.
    """
    def get(self, request, game_id, format=None):
        game = self.get_object(game_id)
        players = game.players.all()
        serializer = PlayerSerializer(players, many=True)
        return Response(serializer.data)

    def post(self, request, game_id, format=None):
        game = self.get_object(game_id)

        # Validation
        if (game.status != 'NS'):
            return Response(data=json.dumps({'details': 'Cannot add player once game has started.'}))

        # Create Player and add to Game
        serializer = PlayerSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(game=game, current_player=False, order=self.determine_order(game))
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def determine_order(self, game):
        query = game.players.all().aggregate(Max('order'))
        if query['order__max'] != None:
            return query['order__max'] + 1
        return 0;

class PlayerDetail(GameView):
    """
    Return player details.
    """
    def get(self, request, game_id, player_name, format=None):
        game = self.get_object(game_id)

        # Validation
        try:
            player = game.player.get(name=player_name)
        except Player.DoesNotExist:
            raise Http404

        serializer = PlayerSerializer(player)
        return Response(serializer.data)





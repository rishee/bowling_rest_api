from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from django.http import Http404
from games.models import Game
from games.models import Frame
from games.serializers import GameSerializer
import json

# @csrf_exempt
class GameList(APIView):
    """
    List all games, or create a new game.
    """
    def get(self, request, format=None):
        games = Game.objects.all()
        serializer = GameSerializer(games, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        game = Game(status='NS')
        game.save()
        serializer = GameSerializer(game)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

class GameView(APIView):
    """
    Retrieve a game instance.
    """
    def get_object(self, pk):
        try:
            return Game.objects.get(pk=pk)
        except Game.DoesNotExist:
            raise Http404

class GameDetail(GameView):
    """
    Return game details.
    """
    def get(self, request, pk, format=None):
        game = self.get_object(pk)
        serializer = GameSerializer(game)
        return Response(serializer.data)

class StartGame(GameView):
    """
    Start a game.
    """
    def post(self, request, pk, format=None):
        game = self.get_object(pk)

        # Validation
        if game.status != "NS":
            return Response(data=json.dumps({'error': 'Game has already started.'}), status=status.HTTP_400_BAD_REQUEST)

        if len(game.players.all()) < 1:
            return Response(data=json.dumps({'error': 'Game must have at least one player before it can be started.'}), status=status.HTTP_400_BAD_REQUEST)

        # Start game and make first player the current player
        game.status = 'IP'
        game.save()
        game.players.filter(order=0).update(current_player=True)

        # Create first frame for each player
        for player in game.players.all():
            frame = Frame(player=player, number=1, status='IP', score=0, rolls_applied=0)
            frame.save()

        player = game.players.get(current_player=True)
        return Response(json.dumps({'next roll': player.name}), status=status.HTTP_200_OK)

import json

from rest_framework import status
from rest_framework.response import Response
from django.db.models import Max

from games.views.game_views import GameView
from games.models import Frame


# @csrf_exempt
class ApplyRoll(GameView):
    """
    Apply a roll to a game
    """
    def post(self, request, game_id, pins, format=None):

        game = self.get_object(game_id)
        pins = int(pins)

        # Validation
        if game.status != 'IP':
            return Response(data=json.dumps({'error': 'Game must be in progress to apply roll.'}), status=status.HTTP_400_BAD_REQUEST)

        if pins > 10 or pins < 0:
            return Response(data=json.dumps({'error': 'Invalid roll.'}), status=status.HTTP_400_BAD_REQUEST)

        # Find current frame and apply roll to it
        player = game.players.get(current_player=True)
        frame = player.frames.get(status='IP')
        try:
            frame.apply_roll(pins)
        except Exception, e:
            return Response(json.dumps({'message': str(e)}), status=status.HTTP_400_BAD_REQUEST)

        # Apply roll to player's other Unscored frames
        for unscored_frame in player.frames.filter(status='US'):
            unscored_frame.apply_roll(pins)
            unscored_frame.close_if_finished()

        # End the frame if it's finished
        if frame.close_if_finished():

            # Special case for 10th frame
            if frame.number == 10:
                player.current_player = False
                player.status = 'F'
                player.save()

                # Check if game is over
                if (player.order == game.players.aggregate(Max('order'))['order__max']):
                    game.status = 'F'
                    game.save()

                    return Response(json.dumps({'note': 'The game has finished.'}), status=status.HTTP_200_OK)

            else:
                # Create a new frame
                new_frame = Frame(player=player, number=frame.number+1, status='IP', score=0, rolls_applied=0)
                new_frame.save()

                # Switch current player to next player
                player.current_player = False;
                player.save()

            player = self.get_next_player(game, player)
            player.current_player = True
            player.save()

        return Response(json.dumps({'next roll': player.name}), status=status.HTTP_200_OK)

    def get_next_player(self, game, player):
        order = player.order + 1
        if order > game.players.aggregate(Max('order'))['order__max']:
            order = 0

        return game.players.get(order=order)

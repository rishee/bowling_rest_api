# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0005_auto_20150223_0510'),
    ]

    operations = [
        migrations.CreateModel(
            name='Roll',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pins_dropped', models.PositiveSmallIntegerField()),
                ('frame', models.ForeignKey(related_name='rolls', to='games.Frame')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='frame',
            name='player',
            field=models.ForeignKey(related_name='frames', to='games.Player'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='frame',
            unique_together=set([('player', 'number')]),
        ),
    ]

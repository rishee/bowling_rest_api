# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0006_auto_20150223_2247'),
    ]

    operations = [
        migrations.AddField(
            model_name='frame',
            name='rolls_applied',
            field=models.PositiveSmallIntegerField(default=0),
            preserve_default=False,
        ),
    ]

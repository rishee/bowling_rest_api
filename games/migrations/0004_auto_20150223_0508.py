# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0003_remove_player_order'),
    ]

    operations = [
        migrations.AddField(
            model_name='player',
            name='current_player',
            field=models.BooleanField(default=False),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='player',
            name='order',
            field=models.PositiveSmallIntegerField(default=1),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='player',
            name='game',
            field=models.ForeignKey(related_name='players', to='games.Game'),
            preserve_default=True,
        ),
    ]

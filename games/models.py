from django.db import models
from django.db.models import Sum
from rest_framework.response import Response
import json

class Game(models.Model):

    STATUSES = (
        ('NS', 'Not Started'),
        ('IP', 'In Progress'),
        ('F', 'Finished')
    )

    status = models.CharField(max_length=2, choices=STATUSES)

class Player(models.Model):

    STATUSES = (
        ('IP', 'In Progress'),
        ('F', 'Finished')
    )

    game = models.ForeignKey(Game, related_name='players')
    status = models.CharField(max_length=2, choices=STATUSES)
    name = models.CharField(max_length=50)
    order = models.PositiveSmallIntegerField()
    current_player = models.BooleanField()

    class Meta:
        unique_together = (("game", "order"),)

class Frame(models.Model):

    STATUSES = (
        ('IP', 'In Progress'),
        ('US', 'Unscored'),
        ('S', 'Scored')
    )

    player = models.ForeignKey(Player, related_name='frames')
    number = models.PositiveSmallIntegerField()
    status = models.CharField(max_length=2, choices=STATUSES)
    score = models.PositiveSmallIntegerField()
    rolls_applied = models.PositiveSmallIntegerField()

    class Meta:
        unique_together = (("player", "number"),)

    def apply_roll(self, pins):

        if self.status == 'IP':

            previous_pins_dropped = self.rolls.aggregate(Sum('pins_dropped'))['pins_dropped__sum']
            if not previous_pins_dropped:
                previous_pins_dropped = 0

            max_pins = 10

            # Validation

            # Special case for 10th frame
            if (self.number == 10):
                if self.rolls_applied > 0:
                    if self.rolls.all()[:1].get().pins_dropped == 10:
                        max_pins = 30
                    elif self.rolls_applied > 1:
                        if self.rolls.all()[:2].aggregate(Sum('pins_dropped'))['pins_dropped__sum'] == 10:
                            max_pins = 20

            if (previous_pins_dropped + pins) > max_pins:
                raise Exception('Invalid number of pins.')

            # Create a Roll and add it to this frame
            roll = Roll(pins_dropped=pins, frame=self)
            roll.save()
            self.score += pins
            self.rolls_applied += 1

        elif self.status == 'US':

            # Add to this frames score but don't create a Roll for it
            self.score += pins
            self.rolls_applied += 1

        self.save()

    def close_if_finished(self):

        pins_dropped = self.rolls.aggregate(Sum('pins_dropped'))['pins_dropped__sum']

        if self.status == 'IP':

            # Special case for 10th frame
            if self.number == 10:
                if self.rolls_applied > 2 or (self.rolls_applied == 2 and pins_dropped < 10):
                    self.status = 'S'

                    self.save()
                    return True

                return False

            else:
                # If strike/spare, set to Unscored so that later rolls will be applied. Otherwised, set to Scored.
                if pins_dropped >= 10:


                    self.status = 'US'

                    self.save()
                    return True

                if self.rolls_applied > 1:
                    self.status = 'S'

                    self.save()
                    return True

                return False

        elif self.status == 'US':
            # If three scores have been applied to this Unscored frame, set it to Scored
            if self.rolls_applied > 2:
                self.status = 'S'

                self.save()
                return True

            return False


class Roll(models.Model):

    frame = models.ForeignKey(Frame, related_name='rolls')
    pins_dropped = models.PositiveSmallIntegerField();
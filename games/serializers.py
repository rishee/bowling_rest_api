from rest_framework import serializers
from games.models import *
from django.db.models import Sum

class RollSerializer(serializers.ModelSerializer):

    class Meta:
        model = Roll
        fields = ('pins_dropped',)

class FrameSerializer(serializers.ModelSerializer):

    rolls = RollSerializer(many=True, read_only=True)

    class Meta:
        model = Frame
        # fields = ('number', 'status', 'rolls')
        fields = ('number', 'status', 'rolls', 'score')

class PlayerSerializer(serializers.ModelSerializer):

    score = serializers.SerializerMethodField()
    frames = FrameSerializer(many=True, read_only=True)

    class Meta:
        model = Player
        fields = ('order', 'name', 'current_player', 'score', 'frames')
        read_only_fields = ('order', 'current_player')

    def get_score(self, obj):
        sum = obj.frames.filter(status='S').aggregate(Sum('score'))['score__sum']
        return sum if sum else 0

class GameSerializer(serializers.ModelSerializer):

    players = PlayerSerializer(many=True, read_only=True)

    class Meta:
        model = Game
        fields = ('id', 'status', 'players')
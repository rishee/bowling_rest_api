from django.conf.urls import url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from games.views import game_views, player_views, roll_views

urlpatterns = [

    # Games
    url(r'^games/$', game_views.GameList.as_view()),
    url(r'^games/(?P<pk>[0-9]+)/$', game_views.GameDetail.as_view()),
    url(r'^games/(?P<pk>[0-9]+)/start', game_views.StartGame.as_view()),

    # Players
    url(r'^games/(?P<game_id>[0-9]+)/players', player_views.PlayerList.as_view()),
    url(r'^games/(?P<game_id>[0-9]+)/(?P<player_name>[A-Z]+)', player_views.PlayerDetail.as_view()),

    # Rolls
    url(r'^games/(?P<game_id>[0-9]+)/roll/(?P<pins>[0-9]+)', roll_views.ApplyRoll.as_view())

]

urlpatterns += staticfiles_urlpatterns()
from django.contrib import admin
from games.models import Game
from games.models import Player
from games.models import Frame
from games.models import Roll

admin.site.register(Game)
admin.site.register(Player)
admin.site.register(Frame)
admin.site.register(Roll)
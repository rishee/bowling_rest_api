# README #

This is a REST API that can be used to track the scoring of multiple bowling games concurrently. It was written using the [Django REST Framework](http://www.django-rest-framework.org/).

### Setup ###

* Install Python 2.7. Use [Virtual Environments](http://docs.python-guide.org/en/latest/dev/virtualenvs/) if you'd like to isolate your Python environment.
* Install [pip](https://pip.pypa.io/en/latest/installing.html).
* Download the source for this repository.
* From the main directory of the local repository, install the project dependencies using `pip install -r requirements.txt`.
* From the main directory, create your local database instance using `python manage.py migrate`.
* From the main directory, run the local Django server using `python manage.py runserver`.
* Utilize the HTTP client of your choice to send requests to the local server (127.0.0.1:8000/).

### Usage ###

* For detailed information on API usage, run the local server and navigate in your browser to the [API Documentation](http://127.0.0.1:8000/static/api.html).

### Status ###

* The code has been tested, but not been prepared for deployment.  It should only be used locally at the moment.